var getCMSContentUrlBase = '/getCMSContent/';
var pmd; /*The current folder*/
var selectedFileId; /*the existing file which is selected to be updated*/
var picker;
uFilesMap = Class.create();
uFilesMap.prototype =
{
    initialize : function( )
    {
      this.filesmap = new Object();
    },
    
    add : function( file )
    {
      this.filesmap[file.Id] = file;
    },
    
    get : function( fileId )
    {
      return this.filesmap[fileId];
    },

    setCurrentFile : function ( fileId, uiUpdate, isFolder ){
     var oldCurrentFileId;
     if ( isFolder ) {
        oldCurrentFileId = pmd;
        pmd = fileId;
     }else{
        oldCurrentFileId = selectedFileId;
        selectedFileId = fileId;
     }
     
      if ( uiUpdate != null ) {
          uiUpdate( fileId, oldCurrentFileId );
      }
    },
    
    selectCMSItems : function(){
        var selectedFiles = new Array();
        for (var key in this.filesmap) {
            var file = this.filesmap[key];
            if ( file.selected ) {
                selectedFiles.push(file.Id);
            }
        }
        return selectedFiles;
    }
};

var globalFilesMap = new uFilesMap();

uFile = Class.create();
uFile.prototype =
  {
      initialize : function( jsonObject )
      {
        this.Id = jsonObject.fileId;
        this.name = jsonObject.filename;
        this.partentId = jsonObject.parentId;
        this.size = jsonObject.fileSize;
        this.type = jsonObject.fileType;
        this.selected = false;
        this.owner = jsonObject.owner;
      },
      
      getUrl : function()
      {
        return getCMSContentUrlBase+this.Id;
      },
      
      addChildren : function( child )
      {
        if (this.type == 'Folder') {
            if (typeof this.childrenIds === "undefined") {
              this.childrenIds = new Object();
             }
        this.childrenIds[child.Id] = child;
        }
      }
  };

uLocalFile = Class.create();
uLocalFile.prototype =
  {
     initialize : function(  )
      {
        if (window.File && window.FileReader && window.FileList && window.Blob) {
        // Great success! All the File APIs are supported.
        this.fullName = '';
        //this.chunkSize = 1024*1024;
        this.chunkSize = 100;
        this.chunkIndex = 0;
        } else {
           alert('The File APIs are not fully supported in this browser.');
        }

      },
      
      emit : function()
      {

      },
      
      upload : function( file, folderId )
      {
        var modifiedDate = file.lastModifiedDate;
        
        
        var remaining = file.size;
        var start;
        var stop;
        while (remaining > 0) {
           start = this.chunkIndex*this.chunkSize;
           var sliceSize = (this.chunkSize > remaining) ? remaining : this.chunkSize;
           stop = start + sliceSize -1; 
           var reader = new FileReader();
           reader.index = this.chunkIndex;
           reader.onloadend = function(evt) {
            if (evt.target.readyState == FileReader.DONE) { // DONE == 2
            var slice = evt.target.result;
            var idx = this.index;
            alert( slice );
            }
          };
        //Slicing the file 
        var blob = file.slice(start, stop);
        reader.readAsBinaryString(blob);
        
        this.chunkIndex++;
        remaining = file.size - this.chunkIndex*this.chunkSize;
        } //End of loop
        this.chunkIndex = 0;
      }
  };