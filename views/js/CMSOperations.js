
selectFolder = function( folderId ){
    globalFilesMap.setCurrentFile( folderId, function( folderId, oldCurrentFolderId)
                                    {
                                        if ( oldCurrentFolderId != null )
                                        {
                                          var oldCurrentFolder = globalFilesMap.get( oldCurrentFolderId );
                                          document.getElementById( 'folderNameSpan'+oldCurrentFolder.Id).innerHTML = oldCurrentFolder.name+' (owner: '+oldCurrentFolder.owner+')';
                                          document.getElementById( 'folderOperation_'+oldCurrentFolder.Id).style.display = 'none';
                                        }
                                        var currentFolder = globalFilesMap.get( folderId );
                                        document.getElementById( 'folderNameSpan'+currentFolder.Id).innerHTML = '<b>'+currentFolder.name+' (owner: '+currentFolder.owner+')</b>';
                                        document.getElementById( 'folderOperation_'+currentFolder.Id).style.display = 'inline';
                                        
                                    }, true);
};

initAreaHtml = function ( folderId, folderName, folderOwner, indentValue ){
       var lMargin = indentValue*15;
       var initHtml = '<img src="'+CMSUrlBase+'/image/plus.gif" alt="Expand" id="folderOperation'+folderId
                      +'" class="unselected" onclick="toggleFolder(\''+folderId+'\', \''+folderName+'\', \''+folderOwner+'\');">'
                      
                      +getDeleteButton( folderId ) 
                      
                      +'<span id="folderNameSpan'+folderId+'" onclick="selectFolder(\''+folderId+'\');">'+folderName
                      +' (owner: '+folderOwner+')</span><span style="display: none;" id="folderOperation_'+folderId+'"><input type=\'text\' id="'
                      +folderId+'_subDirname"><input type="button" value="mkdir" onclick="mkSubDir( \''
                      +folderId+'\' )" /><input type="file" id="uploadfile'
                      +folderId+'"/><input type="button" value="upload" onclick="upload(\''
                      +folderId+'\')"/> As the owner: <input type="text" id="owner_'+folderId+'" size="25"/></span><BR>';
           initHtml += "<div style='padding-left: "+lMargin+"px;' id='"+folderId+"subDiv'></div><input type='hidden' id='indent"
                    +folderId+"' value='"+indentValue+"'>";
           return initHtml;
};


getFileDiv = function ( file ){
    return getDeleteButton( file.Id ) + '<a href="'+CMSUrlBase+file.getUrl()+'" target="new">'+file.name+'</a>  (owner: '+file.owner+')<BR>';
};

getFolderDiv = function ( folder, indent ){
    return "<div id='"+folder.Id+"Area'>"+initAreaHtml(folder.Id, folder.name, folder.owner, indent)+"</div>";
};

getDeleteButton = function ( fileId )
{
    return '<img src="'+CMSUrlBase+'/image/file_delete.png" alt="Delete" id="deleteFile'+fileId
                      +'" onclick="deleteFile(\''+fileId+'\');">'
};

mkSubDir = function ( parentId )
{
       var folderName = document.getElementById( parentId+'_subDirname').value;

       if (folderName.length === 0 || !folderName.trim())
       {
          alert('The folder name is required.');
          return;
       }
       'owner_'+parentId
       var owner = document.getElementById('owner_'+parentId).value;
       comm.postMkdir( parentId, folderName, owner, function( data ){
            loadChildren ( parentId );
            document.getElementById( parentId+'_subDirname').value='';
        });
};

postSampleToken = function ( token )
{
	var tokenObj = new uToken( );
	//The user can set the unique token explicitly, or the server sets token for user.
	tokenObj.Token = token;
    var rules = new Object();
	//The list of the file/folder ids on which the token is to be applied on. 
	//The items can be either file(s) which specified by the field Id, or files under folder which specified by the field folderId.
	//In case the folder id is specified, only the immediate children are covered by default unless the field recursive is set as true, 
	//therefore all the children under the folder are covered even they are under sub folders. 
	//Optionally the owner can be specified by the owner field, or the token can be passed with any owner.
	//The items is a required field and at least one item (Id or folderId) has to be set. 
    rules.items = {"objects":[{"folderId":"root"}, {"folderId":"EF14101299446855", "recursive":"true"}, {"Id":"EF14099519036210"}], "owner":""};

	//The list of valid/invalid IP addresses or computer names associated with the token. The valid field indicates if the addresses/names are valid or invalid.
	//The default value of the valid field is true, therefore the token will be passed only from the IPs/names within the range. While the token will be passed 
	//only from the IPs/names NOT within the range if the valid field is false. The IPs in the range can be a single address, or a range of address which
	//has * to represent any string without dot(.). The addresses can be either v4 or v6. 
	//The computerNames can be single computer or computer name with domain, same as Ip addresses, the * sign represents any string  without dot(.).
	//The IPNameRange is optional. If IPNameRange is not set, the token is applied to any Ip/name.
    rules.IPNameRange = {"valid":"true", "IPs":["127.0.0.1","192.168.*.*","2001:0db8:3c4d:0015:*:*:*:*"], "computerNames":["computer1", "*.mydomain.com"]};
	
	tokenObj.rules = rules;

    //Optionally specify the start date of the token. The token is in affect after the start date if set.
	var startD = new Date( );
	startD.setFullYear(2000);
	tokenObj.startDate = startD;

    //Optionally specify the end date of the token. The token is NOT in affect after the end date if set.
	var endD = new Date( );
	endD.setFullYear(2050);
	tokenObj.endDate = endD;

    comm.postToken( tokenObj, function( data ){
    	    var feedBack = JSON.parse(data);
    	    if (feedBack.status == 'OK')
    	    	{
    	          var postedToken = feedBack.postedToken;
                  alert(postedToken.Token);
    	    	}else{
    	    		alert(feedBack.msg);
    	    	}
        });
};


getTokenSample = function ( token )
{
    comm.getToken(token ,function( data ){
      var feedback = JSON.parse(data);
      if ( feedback.status == "ERROR") {
            alert( feedback.msg );
            }else{  
             alert (feedback.tokenObj.rules);
          }
     })
};

upload = function ( parentId )
{
    var fileDiv = 'uploadfile'+parentId;
    var owner = document.getElementById('owner_'+parentId).value;
     comm.postuploadFile(parentId, fileDiv ,function( data ){
        document.getElementById( fileDiv ).value = '';
        loadChildren ( parentId );
        }, null, owner);
};

deleteFile = function(fileId)
{
    var fileBeingDeleted = globalFilesMap.get( fileId );
    var parentId = fileBeingDeleted.partentId;
    var result = confirm( "Theis operation may remove "+fileBeingDeleted.name+" permanently." );
    if (result==true) {
        comm.deleteCMSFile(fileId ,function( data ){
            var feedback = JSON.parse(data);
            if ( feedback.status == "ERROR") {
                alert( feedback.msg);
            }else{
              loadChildren( parentId ); 
            }
        })
    }
};

getFileByPath = function( )
{
	var fullPath = document.getElementById('fpath').value;
    comm.getFileByPath(fullPath ,function( data ){
      var feedback = JSON.parse(data);
      if ( feedback.status == "ERROR") {
            document.getElementById( 'getFileByPathResult').innerHTML = feedback.msg;
            }else{  
             var fileJson = feedback.fileJson;
             var fileObj = new uFile( fileJson );
             var resultMsg = "<div>file name: "+fileObj.name+"</div>";
             resultMsg = resultMsg+"<div>file Id: "+fileObj.Id+"</div>";
             resultMsg = resultMsg+"<div>file owner: "+fileObj.owner+"</div>";
             if ( fileObj.type != 'Folder')
            	 {
                   resultMsg = resultMsg+"<div>file URL: "+CMSUrlBase+fileObj.getUrl()+"</div>";
            	 }
             document.getElementById( 'getFileByPathResult').innerHTML = resultMsg;
          }
     })
    
};

emptyTrash = function( )
{
    comm.deleteTrash(function( data ){
      var feedback = JSON.parse(data);
      alert(feedback.msg);
     })
};

prepareRemove = function()
{
    var selected = globalFilesMap.selectCMSItems();
    if ( selected == null || selected.length == 0) {
        alert('Please select at least ONE file/folder to delete.');
    }
    else{
     comm.getCMSLockForDeletion( selected, confirmDeletionUponLock);
    }
};

confirmDeletionUponLock = function( data )
{
    alert( data );
};

handleFileSelect =  function(evt) {
    evt.stopPropagation();
    evt.preventDefault();

    var files = evt.dataTransfer.files; // FileList object.
    // files is a FileList of File objects. List some properties.
    
    //var output = [];
    var folderId = this.id.replace("droparea_","");
    for (var i = 0, f; f = files[i]; i++) {
        localFile.upload( f, folderId);      
    }
  };

handleDragOver = function (evt) {
    evt.stopPropagation();
    evt.preventDefault();
    evt.dataTransfer.dropEffect = 'copy'; // Explicitly show this is a copy.
  };