var comm = new uComm();
var localFile = new uLocalFile();
var defaultOwner = 'anonymous';
    
loadRoot = function( rootId, rootName ){
   var rootInitHtml = initAreaHtml( rootId, rootName, defaultOwner, 1 );
   document.getElementById(rootId+'Area').innerHTML = rootInitHtml;

   rootJsonObject = new Object();
   rootJsonObject.fileId = 'root';
   rootJsonObject.filename = 'root';
   rootJsonObject.parentId = null;
   rootJsonObject.fileSize = '0';
   rootJsonObject.fileType = 'Folder';
   rootJsonObject.owner = defaultOwner;
   rootFolder = new uFile(rootJsonObject);

   globalFilesMap.add( rootFolder );
   selectFolder( rootFolder.Id );
};

toggleFolder = function( folderId, folderName, folderOwner, type ){
    var folderOperationSrc = document.getElementById('folderOperation'+folderId).src;
    if ( folderOperationSrc.indexOf('/image/plus.gif') > -1) {
        loadChildren(folderId, type);
    }else{
        var indent = document.getElementById('indent'+folderId).value;
        document.getElementById(folderId+'Area').innerHTML = initAreaHtml( folderId, folderName, folderOwner, indent );
    }
   selectFolder( folderId );
};

loadChildren = function( parentId, type ){
        comm.getCMSChildren( parentId, type, function( data ){
        var cmsChildren = JSON.parse(data);
        var filesJson = cmsChildren.filesJson;
        var children='';
        var indent = parseInt(document.getElementById('indent'+parentId).value)+1;
        
        var parentFolder = globalFilesMap.get( parentId );
            for (var i = 0; i < filesJson.length; i++) {
               var cmsFile = new uFile( filesJson[i] );
               globalFilesMap.add( cmsFile );
               parentFolder.addChildren( cmsFile );
              if ( cmsFile.type == 'Folder') {
                children += getFolderDiv( cmsFile, indent );
              }else{
                children += getFileDiv(cmsFile);
              }
            }
        document.getElementById("folderOperation"+parentId).src = CMSUrlBase+"/image/minus.gif";
        document.getElementById(parentId+'subDiv').innerHTML = children;
        } );
};
  
uploadFileStream = function( fullFileName){
   
};