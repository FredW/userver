var scriptFileType = config.CMSScriptFileDataType;

selectFolder = function( folderId ){
    globalFilesMap.setCurrentFile( folderId, function( folderId, oldCurrentFolderId)
                                    {
                                        document.getElementById( 'folderNameSpan'+folderId).classList.add('currentFolder');
                                        if ( oldCurrentFolderId != null && oldCurrentFolderId != folderId)
                                        {
                                          var oldCurrentFolder = globalFilesMap.get( oldCurrentFolderId );
                                          document.getElementById( 'folderNameSpan'+oldCurrentFolder.Id).classList.remove('currentFolder');
                                        }
                                    }, true);
};

initAreaHtml = function ( folderId, folderName, indentValue ){
       var lMargin = indentValue*10;
       var initHtml = '<img src="/image/plus.gif" alt="Expand" id="folderOperation'+folderId
                      +'" class="unselected" onclick="toggleFolder(\''+folderId+'\', \''+folderName+'\', \''+scriptFileType+'\');">'
                      +'<span id="folderNameSpan'+folderId+'" onclick="selectFolder(\''+folderId+'\');">'+folderName+'</span><BR>';
           initHtml += "<div style='padding-left: "+lMargin+"px;' id='"+folderId+"subDiv'></div><input type='hidden' id='indent"+folderId+"' value='"+indentValue+"'>";
           return initHtml;
};

getFolderDiv = function ( folder, indent ){
    return "<div id='"+folder.Id+"Area'>"+initAreaHtml(folder.Id, folder.name, indent)+"</div>";
};

getFileDiv = function ( file ){
    return '<span onclick= "chooseFile(\''+file.Id+'\')";>'+file.name+'</span><BR/> ';
};

chooseFile = function( fileId )
{
    if (fileId === undefined || fileId === null) {
            globalFilesMap.setCurrentFile( fileId, function( fileId, oldCurrentfileId)
                                    {
                                        document.getElementById( 'fileName').value = null;
                                    }, false);
    }else {
    var currentFile = globalFilesMap.get( fileId );
    globalFilesMap.setCurrentFile( fileId, function( fileId, oldCurrentfileId)
                                    {
                                        document.getElementById( 'fileName').value = currentFile.name;
                                    }, false);
    selectFolder( currentFile.partentId);
    }
};

saveScript = function( )
{
  var method = 'post';
  var fileNamearea = document.getElementById( 'fileName');
  var fileName = fileNamearea.value;
  if ( fileName == null || fileName === "") {
    fileNamearea.focus();
    alert("Please specify the file name.");
    return;
  }
  
  folder = globalFilesMap.get(pmd);
  for (var key in folder.childrenIds) {
    file = folder.childrenIds[key];
    if (file.name == fileName){
        method = 'put';
        break;
    }
  }

  var contentBody = localStorage.getItem(config.CMSFileDataKey);
  var type = localStorage.getItem(config.CMSFileTypeKey);
  comm.uploadScript( pmd, fileName, method, contentBody, type, function( data ){
         
  var jsonObject = JSON.parse(data);
  var cmsFile = new uFile( jsonObject.fileJson );
  window.opener.CMSPickerCallBack(cmsFile);
 } );
};
