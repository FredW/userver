uToken = Class.create();
uToken.prototype =
  {
      initialize : function( jsonObject )
      {
    	if ( jsonObject )
    		{
              this.Token = jsonObject.Token;
              this.rules = jsonObject.rules;
              this.startDate = jsonObject.startDate;
              this.endDate = jsonObject.endDate;
    		}
      }
  };