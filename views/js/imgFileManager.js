var imgFileType = config.imageTypes;

selectFolder = function( folderId ){
    globalFilesMap.setCurrentFile( folderId, function( folderId, oldCurrentFolderId)
                                    {
                                        document.getElementById( 'folderNameSpan'+folderId).classList.add('currentFolder');
                                        if ( oldCurrentFolderId != null && oldCurrentFolderId != folderId)
                                        {
                                          var oldCurrentFolder = globalFilesMap.get( oldCurrentFolderId );
                                          document.getElementById( 'folderNameSpan'+oldCurrentFolder.Id).classList.remove('currentFolder');
                                        }
                                    }, true);
};

initAreaHtml = function ( folderId, folderName, indentValue ){
       var lMargin = indentValue*10;
       var initHtml = '<img src="/image/plus.gif" alt="Expand" id="folderOperation'+folderId
                      +'" class="unselected" onclick="toggleFolder(\''+folderId+'\', \''+folderName+'\', \''+imgFileType+'\');">'
                      +'<span id="folderNameSpan'+folderId+'">'+folderName+'</span><BR>';
           initHtml += "<div style='padding-left: "+lMargin+"px;' id='"+folderId+"subDiv'></div><input type='hidden' id='indent"+folderId+"' value='"+indentValue+"'>";
           return initHtml;
};

loadChildren = function ( folderId )
{
    comm.getCMSChildren( folderId, imgFileType, function( data ){
    var cmsChildren = JSON.parse(data);
    var filesJson = cmsChildren.filesJson;
    var childrenFiles='';
    var childrenFolders='';
            for (var i = 0; i < filesJson.length; i++) {
               var cmsFile = new uFile( filesJson[i] );
              if ( cmsFile.type == 'Folder') {
                childrenFolders += getFolderDiv( cmsFile );
              }else{
                childrenFiles += getFileDiv(cmsFile);
              }
            }
        document.getElementById('fileList').innerHTML = childrenFolders+childrenFiles;
        } );
};

getFolderDiv = function( folder, image)
{
    return getFolderDivByIdAndName(folder.Id, folder.name, image);
}

getFolderDivByIdAndName = function( folderId, folderName, image)
{
    return "<div class='folder' id='"+folderId+"Folder'><a href='/getCMSExplorer/imgFileManager?folder="+folderId+"'><img src='/image/folder.jpg' height='20' width='30'>"+folderName+"</a></div>";
}

/*
getFolderDiv = function ( folder, indent ){
    return "<div id='"+folder.Id+"Area'>"+initAreaHtml(folder.Id, folder.name, indent)+"</div>";
};
*/
getFileDiv = function ( file ){
    return "<div class='preview'><img src='"+file.getUrl()+"' alt='"+file.name+"' height='120' width='150'></div>";
};

preview = function ( fileId )
{
    var currentFile = globalFilesMap.get( fileId );
    document.getElementById( 'previewArea').src = currentFile.getUrl();
    document.getElementById( 'selectedUrl').value = currentFile.getUrl();
    document.getElementById( 'fileName').value = currentFile.name;
};
