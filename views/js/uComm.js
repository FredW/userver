//The module hadnls the communitcation between the front end and the back end
var sessionCookieName = "u3dsessionkey";
var sessionKey;
var restReq;

var uComm = Class.create({
    initialize : function() {
        cookieOperator = new uCookieOperator();
           if (cookieOperator.areCookiesEnabled()) {
             savedSessionKey = cookieOperator.readCookie( sessionCookieName );
             if ( savedSessionKey != null ) {
               sessionKey = savedSessionKey;
             }
           }
    },
    
    getSession : function(){
        Ext.Ajax.request({
            url: '/U3D/getSession',
            params: {
                sessionid: sessionid
            },
            method: 'POST',
            success: function(response) {
                var jsonObj = Ext.decode(response.responseText);
                var scriptId = jsonObj.scriptId;
                console.log('the script was saved. The id is ' + scriptId);
            },
            failure: function() {
                console.log('woops');
            }
        });
    },

   getRestRequest: function () {
              if (restReq === undefined || restReq === null) {
                if (window.XMLHttpRequest)
                {// code for IE7+, Firefox, Chrome, Opera, Safari
                   restReq=new XMLHttpRequest();
                 }
                else
                {// code for IE6, IE5
                  restReq=new ActiveXObject("Microsoft.XMLHTTP");
                }
          }
         return restReq;
    },

    getRestCall  : function( path, successCallBack, unSuccessCallBack ){
        var req = this.getRestRequest();
        var url = config.rest_root_url + path;
        req.open("GET", url, true);
        // Create the callback:
        req.onreadystatechange = function() {
          if (req.readyState != 4) return; // Not there yet
          if (req.status != 200) {
            // Handle request failure here...
            if ( unSuccessCallBack != null )
            {
                unSuccessCallBack( req );
            }
            return req.status;
          }
          // Request successful, read the response
          var resp = req.responseText;
          successCallBack(resp);
        };
        req.send();
    },
    
    postuploadFile: function( parentId, fileDiv, successCallBack, fileId, owner, unSuccessCallBack ){
        var files = document.getElementById( fileDiv );
        var file = files.files[0];
        this.postUpload( parentId, file, successCallBack, fileId, owner, unSuccessCallBack );
    },

    postUpload: function( parentId, file, successCallBack, fileId, owner, unSuccessCallBack ){
        var req = this.getRestRequest();
        var postParameters = {fileId: fileId, parentId: parentId, owner: owner};
        var postParametersStr = JSON.stringify(postParameters);
        /* Create a FormData instance */
        var formData = new FormData();
         /* Add the file */ 
        formData.append("upload", file);
        req.open("post", CMSUrlBase+"/uploadFile?para="+postParametersStr, true);
        req.onreadystatechange = function() {
          if (req.readyState != 4) return; // Not there yet
          if (req.status != 200) {
            // Handle request failure here...
            if ( unSuccessCallBack != null )
            {
                unSuccessCallBack( req );
            }
            return req.status;
          }
          // Request successful, read the response
          var resp = req.responseText;
          successCallBack(resp);
        };
        req.send(formData);  /* Send to server */ 
    },
    
    getFileByPath: function( fullpath, successCallBack, unSuccessCallBack ){
        var req = this.getRestRequest();
        var postParameters = {fullpath: fullpath};
        var postParametersStr = JSON.stringify(postParameters);
        req.open("GET", CMSUrlBase+"/getFileByPath?para="+postParametersStr, true);
        // Create the callback:
        req.onreadystatechange = function() {
          if (req.readyState != 4) return; // Not there yet
          if (req.status != 200) {
            // Handle request failure here...
            if ( unSuccessCallBack != null )
            {
                unSuccessCallBack( req );
            }
            return req.status;
          }
          // Request successful, read the response
          var resp = req.responseText;
          successCallBack(resp);
        };
        req.send();
    },

    deleteTrash:function(successCallBack, unSuccessCallBack){
    	var req = this.getRestRequest();
    	req.open("delete", CMSUrlBase+"/deleteTrash", true);
        req.onreadystatechange = function() {
            if (req.readyState != 4) return; // Not there yet
            if (req.status != 200) {
              // Handle request failure here...
              if ( unSuccessCallBack != null )
              {
                  unSuccessCallBack( req );
              }
              return req.status;
            }
            // Request successful, read the response
            var resp = req.responseText;
            successCallBack(resp);
          };
          req.send();  /* Send to server */ 
    },

    uploadScript: function( folderId, fileName, method, scriptBody, type, owner, successCallBack, unSuccessCallBack ){
        var req = this.getRestRequest();
        var postParameters = {folderId: folderId, fileName: fileName, scriptBody: scriptBody, type: type, owner: owner};
        var postParametersStr = JSON.stringify(postParameters);
        req.open("POST", CMSUrlBase+"/uploadScript?para="+postParametersStr, true);
        req.onreadystatechange = function() {
          if (req.readyState != 4) return; // Not there yet
          if (req.status != 200) {
            // Handle request failure here...
            if ( unSuccessCallBack != null )
            {
                unSuccessCallBack( req );
            }
            return req.status;
          }
          // Request successful, read the response
          var resp = req.responseText;
          successCallBack(resp);
        };
        req.send();  /* Send to server */ 
    },

    postToken: function( tokenToPost, successCallBack, unSuccessCallBack ){
        var req = this.getRestRequest();
        var postParameters = {Token: tokenToPost.Token, rules: tokenToPost.rules, startDate: tokenToPost.startDate, endDate: tokenToPost.endDate};
        var postParametersStr = JSON.stringify(postParameters);
        req.open("POST", CMSUrlBase+"/postToken?para="+postParametersStr, true);
        req.onreadystatechange = function() {
          if (req.readyState != 4) return; // Not there yet
          if (req.status != 200) {
            // Handle request failure here...
            if ( unSuccessCallBack != null )
            {
                unSuccessCallBack( req );
            }
            return req.status;
          }
          // Request successful, read the response
          var resp = req.responseText;
          successCallBack(resp);
        };
        req.send();  /* Send to server */ 
    },

    getToken: function(token, successCallBack, unSuccessCallBack){
        var req = this.getRestRequest();
        var postParameters = {Token: token};
        var postParametersStr = JSON.stringify(postParameters);
        req.open("GET", CMSUrlBase+"/getToken?para="+postParametersStr, true);
        req.onreadystatechange = function() {
          if (req.readyState != 4) return; // Not there yet
          if (req.status != 200) {
            // Handle request failure here...
            if ( unSuccessCallBack != null )
            {
                unSuccessCallBack( req );
            }
            return req.status;
          }
          // Request successful, read the response
          var resp = req.responseText;
          successCallBack(resp);
        };
        req.send();  /* Send to server */ 
    },
    
    deleteToken: function(token, successCallBack, unSuccessCallBack){
        var req = this.getRestRequest();
        var postParameters = {Token: token};
        var postParametersStr = JSON.stringify(postParameters);
        req.open("DELETE", "/deleteToken?para="+postParametersStr, true);
        req.onreadystatechange = function() {
          if (req.readyState != 4) return; // Not there yet
          if (req.status != 200) {
            // Handle request failure here...
            if ( unSuccessCallBack != null )
            {
                unSuccessCallBack( req );
            }
            return req.status;
          }
          // Request successful, read the response
          var resp = req.responseText;
          successCallBack(resp);
        };
        req.send();  /* Send to server */ 
    },

    postMkdir: function( parentId, folderName, owner, successCallBack, unSuccessCallBack ){
        var req = this.getRestRequest();
        var postParameters = {folderName: folderName, owner: owner, parentId: parentId};
        var postParametersStr = JSON.stringify(postParameters);
        req.open("post", CMSUrlBase+"/mkDir?para="+postParametersStr, true);
        req.onreadystatechange = function() {
          if (req.readyState != 4) return; // Not there yet
          if (req.status != 200) {
            // Handle request failure here...
            if ( unSuccessCallBack != null )
            {
                unSuccessCallBack( req );
            }
            return req.status;
          }
          // Request successful, read the response
          var resp = req.responseText;
          successCallBack(resp);
        };
        req.send();  /* Send to server */ 
    },
    
    getCMSChildren: function( parentId, type, successCallBack, unSuccessCallBack ){
        var req = this.getRestRequest();
        var postParameters = {type: type};
        var postParametersStr = JSON.stringify(postParameters);
        req.open("get", CMSUrlBase+"/getChildren/"+parentId+"?para="+postParametersStr, true);
        req.onreadystatechange = function() {
          if (req.readyState != 4) return; // Not there yet
          if (req.status != 200) {
            // Handle request failure here...
            if ( unSuccessCallBack != null )
            {
                unSuccessCallBack( req );
            }
            return req.status;
          }
          // Request successful, read the response
          var resp = req.responseText;
          successCallBack(resp);
        };
        req.send();  /* Send to server */ 
    },
    
    deleteCMSFile: function( fileId, successCallBack, unSuccessCallBack ){
        var req = this.getRestRequest();
        var postParameters = {fileId: fileId};
        var postParametersStr = JSON.stringify(postParameters);
        req.open("delete", CMSUrlBase+"/deleteFile?para="+postParametersStr, true);
        req.onreadystatechange = function() {
          if (req.readyState != 4) return; // Not there yet
          if (req.status != 200) {
            // Handle request failure here...
            if ( unSuccessCallBack != null )
            {
                unSuccessCallBack( req );
            }
            return req.status;
          }
          // Request successful, read the response
          var resp = req.responseText;
          successCallBack(resp);
        };
        req.send();  /* Send to server */ 
    },
    
    getCMSLockForDeletion: function ( files, successCallBack, unSuccessCallBack){
        var req = this.getRestRequest();
        var putParameters = {files: files};
        var putParametersStr = JSON.stringify(putParameters);
        req.open("put", "/putCMSLocks?para="+putParametersStr, true);
        req.onreadystatechange = function() {
          if (req.readyState != 4) return; // Not there yet
          if (req.status != 200) {
            // Handle request failure here...
            if ( unSuccessCallBack != null )
            {
                unSuccessCallBack( req );
            }
            return req.status;
          }
          // Request successful, read the response
          var resp = req.responseText;
          successCallBack(resp);
        };
        req.send();  /* Send to server */ 
    }
});

var uCookieOperator = Class.create({
    setCookie : function (cname,cvalue,exdays) {
      var d = new Date();
      d.setTime(d.getTime()+(exdays*24*60*60*1000));
      var expires = "expires="+d.toGMTString();
      document.cookie = cname + "=" + cvalue + "; " + expires;
    },
    
    readCookie : function (name) {
      var nameEQ = name + "=";
      var ca = document.cookie.split(';');
      for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') c = c.substring(1, c.length);
        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
      }
        return null;
    },
    
    areCookiesEnabled : function () {
        var r = false;
        this.setCookie("testing", "Hello", 1);
        if (this.readCookie("testing") != null) {
            r = true;
            this.eraseCookie("testing");
        }
        return r;
    },
    
    eraseCookie : function (name) {
        this.setCookie(name, "", -1);
    }
});

