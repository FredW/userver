
/**
 * Module dependencies.
 */

var express = require('express');
var routes = require('./routes');
var user = require('./routes/user');
var uRoute = require('./routes/uRoute');
var uCMSRoute = require('./routes/uCMSRoute');
var uLaunch = require('./routes/uLaunch');
var http = require('http');
var path = require('path');
var passport = require('passport');
var mysql = require('mysql');
var config = require('./config');

var port = config.app_port || 3000;
var app = express();


// all environments
app.set('port', port);
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

// Enables CORS
var enableCORS = function(req, res, next) {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS');
    res.header('Access-Control-Allow-Headers', 'Content-Type, Authorization, Content-Length, X-Requested-With');
 
    // intercept OPTIONS method
    if ('OPTIONS' == req.method) {
      res.send(200);
    }
    else {
      next();
    }
};
app.use(enableCORS);
//app.engine('.html', require('jade').__express);
app.use(express.favicon());
app.use(express.logger('dev'));
app.use(express.json());
app.use(express.urlencoded());
app.use(express.methodOverride());

//The order matters. the following uses MUST be put before use of router.
app.use(express.bodyParser({uploadDir:path.join(__dirname, 'uploads')}));
app.use(express.methodOverride());
app.use(express.multipart());

app.use(app.router);

app.use(require('stylus').middleware(path.join(__dirname, 'public')));

//To serve the static files here
app.use('/', express.static(path.join(__dirname, 'views')));
app.use('/js', express.static(path.join(__dirname, 'views/js')));
app.use('/image', express.static(path.join(__dirname, 'views/image')));
app.use('/stylesheets', express.static(path.join(__dirname, 'views/stylesheets')));
//app.use('/public', express.static(path.join(__dirname, 'public')));

var connection = mysql.createConnection({
    host: config.database_host,
    user: config.database_user,
    password: config.database_password,
    database: config.database_database
});

getConnection = function() {
    return connection;
};

getBasePath = function(){
    return __dirname;  
};

var viewerInterval = config.viewer_interval || 5000;
getViewerInterval = function() {
    return viewerInterval;
};
// development only
if ('development' == app.get('env')) {
    app.configure(function() {
        app.use(express.errorHandler({dumpExceptions: true, showStack: true}));
    });
}

//app.get('/', routes.index);
//app.get('/users', user.list);


// Facebook authenticate
var FacebookStrategy = require('passport-facebook').Strategy;
var FACEBOOK_APP_ID = "681627611862056";
var FACEBOOK_APP_SECRET = "3c21229e89c2ddc87b0abbac8a6190ec";
passport.use(new FacebookStrategy({
    clientID: FACEBOOK_APP_ID,
    clientSecret: FACEBOOK_APP_SECRET,
    callbackURL: "http://www.u3dapp.com:" + port + "/u3d/fbauthview"
},
function(accessToken, refreshToken, profile, done) {
    if (profile.id)
    {
        console.log("profile.id=" + profile.id);
        user.profileId = profile.id;
    }
    return done(null, user);
}
));

// Redirect the user to Facebook for authentication.  When complete,
// Facebook will redirect the user back to the application at
//     /auth/facebook/callback
app.get('/auth/facebook', passport.authenticate('facebook'));

// Facebook will redirect the user to this URL after approval.  Finish the
// authentication process by attempting to obtain an access token.  If
// access was granted, the user will be logged in.  Otherwise,
// authentication has failed.
app.get('/auth/facebook/callback',
        passport.authenticate('facebook', {successRedirect: '/',
    failureRedirect: '/users'}));

app.get('/u3d/testUrl', function(req, res, next) {
    passport.authenticate('facebook', function(err, user, info) {
        if (err) {
            return next(err);
        }
        if (!user) {
            return res.redirect('/users');
        }
        uRoute.testUrl(req, res);
    })(req, res, next);
});

app.get('/u3d/fbauthview', function(req, res, next) {
    var query = require('url').parse(req.url, true).query;
    var scriptId = query.scriptId;
    var userInfoHash = getUserInfoHash(req);
    if (scriptId != undefined)
    {
        if (!global.requiredScriptIds || global.requiredScriptIds == undefined)
        {
            global.requiredScriptIds = {};
        }
        global.requiredScriptIds[userInfoHash] = scriptId;
    }
    passport.authenticate('facebook', function(err, user, info) {
        if (err) {
            return next(err);
        }
        if (!user) {
            return res.redirect('/users');
        }
        executeWithFBAuth(user.profileId, uLaunch.view, req, res);
    })(req, res, next);

});



getUserInfoHash = function(req)
{
    var agent = req.headers['user-agent'];
    var referrer = req.headers['referrer'];
    var ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress;
    return String(agent) + String(referrer) + String(ip);
};
/*
 //Google authenticate
 var GoogleStrategy = require('passport-google-oauth').OAuth2Strategy;
 
 var GOOGLE_CLIENT_ID = '733053724090.apps.googleusercontent.com';
 var GOOGLE_CLIENT_SECRET = 'rhyPFb2Ay8l5gg46U-Jk1AVq';
 
 passport.use(new GoogleStrategy({
 clientID: GOOGLE_CLIENT_ID,
 clientSecret: GOOGLE_CLIENT_SECRET,
 callbackURL: "http://localhost:3000/U3D/gOauthlaunch",
 scope: 'https://www.googleapis.com/auth/userinfo.profile'
 },
 function(accessToken, refreshToken, profile, done){
 if (profile.id)
 {
 console.log("profile.id=" + profile.id);
 user.profileId = profile.id;
 }
 return done(null, user);
 }
 ));
 */

app.get('/U3D/gOauthlaunch', function(req, res, next) {
    passport.authenticate('google', function(err, user, info) {
        if (err) {
            return next(err);
        }

        if (!user) {
            return res.redirect('/users');
        }
        authUserByGoogleId(user.profileId, uLaunch.launch, req, res);
    })(req, res, next);
});

app.get('/U3D/devlaunch', function(req, res, next) {
    if (config.dev_googler_id != undefined)
    {
        authUserByGoogleId(config.dev_googler_id, uLaunch.launch, req, res);
    }
    else
    {
        return res.redirect('/U3D/gOauthlaunch');
    }
});

//temporary path for the viewers without auth
app.get('/U3D/view', function(req, res, next) {
    if (config.dev_googler_id != undefined)
    {
        authUserByGoogleId(config.dev_googler_id, uLaunch.view, req, res);
    }
    else
    {
        return res.redirect('/u3d/fbauthview');
    }
});

app.get('/U3D/getSession', uRoute.getSession);
app.post('/U3D/createScript', uRoute.createScript);
app.post('/U3D/updateScript', uRoute.updateScript);
app.post('/U3D/loadScript', uRoute.loadScript);
app.post('/U3D/addNode', uRoute.addNode);
app.post('/U3D/loadMyRoot', uRoute.loadMyRoot);
app.post('/U3D/loadNodeChildren', uRoute.loadNodeChildren);
app.post('/U3D/loadNodeScripts', uRoute.loadNodeScripts);
app.post('/U3D/loadUpdatedScript', uRoute.loadUpdatedScript);

app.post('/mkDir', uCMSRoute.mkDir);
app.post('/uploadFile', uCMSRoute.uploadfile);
app.post('/uploadScript', uCMSRoute.postScript);
app.post('/postToken', uCMSRoute.postToken);

app.delete('/deleteFile', uCMSRoute.deletefile);
app.delete('/deleteTrash', uCMSRoute.deleteTrash);

app.get('/getCMSContent/:extid', getFile);
app.get('/getChildren/:extid', getChildren);
app.get('/getFileByPath', getFileByPath)
app.get('/getCMSExplorer', uCMSRoute.getExplorer);
app.get('/getCMSExplorer/scriptFilePicker', uCMSRoute.getScriptFilePicker);
app.get('/getCMSExplorer/imgFileManager', uCMSRoute.getImgFileManager);
app.get('/getCMSExplorer/imgFilePicker', uCMSRoute.imgFilePicker);
app.get('/getToken', getToken);
app.get('/getWebCapture', getWebCapture);

app.get('/test/restGet', function(req, res){
    var query = req.query;
    var para = query.para;
    var jsonObj = JSON.parse(para);
    var paraValue1 = jsonObj.para1;
    var paraValue2 = jsonObj.para2;
    result = {status: 'OK', feedback1: paraValue1, feedback2: paraValue2};
    res.send(JSON.stringify(result));
    });

http.createServer(app).listen(app.get('port'), function() {
    console.log('Express server listening on port ' + app.get('port'));
});

