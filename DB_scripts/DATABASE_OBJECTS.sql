/*The DDL*/
CREATE SCHEMA `U3DSchema` ;

USE `U3DSchema`;

CREATE TABLE `users` (
  `idusers` int(11) NOT NULL AUTO_INCREMENT,
  `user_name` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`idusers`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;


CREATE TABLE `ext_auth_ids` (
  `idext_auth_ids` int(11) NOT NULL AUTO_INCREMENT,
  `auth_host` varchar(10) NOT NULL COMMENT 'The host of the ext auth. GG for google, FB for fackbook',
  `id` varchar(45) NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`idext_auth_ids`),
  UNIQUE KEY `UK_id_host` (`auth_host`,`id`),
  KEY `FK_user_id_auth` (`user_id`),
  CONSTRAINT `FK_user` FOREIGN KEY (`user_id`) REFERENCES `users` (`idusers`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `script_nodes` (
  `idscript_nodes` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `description` varchar(200) DEFAULT NULL,
  `parent` int(11) DEFAULT NULL,
  `owner` int(11) NOT NULL,
  PRIMARY KEY (`idscript_nodes`),
  UNIQUE KEY `UK_name_parent` (`name`,`parent`),
  KEY `FK_parent_idx` (`parent`),
  KEY `FK_user_idx` (`owner`),
  CONSTRAINT `FK_owner` FOREIGN KEY (`owner`) REFERENCES `users` (`idusers`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_parent` FOREIGN KEY (`parent`) REFERENCES `script_nodes` (`idscript_nodes`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

CREATE TABLE `scripts` (
  `idscripts` int(11) NOT NULL AUTO_INCREMENT,
  `author` int(11) DEFAULT NULL,
  `name` varchar(45) NOT NULL,
  `description` varchar(200) DEFAULT NULL,
  `script` varchar(1000) CHARACTER SET utf8 DEFAULT NULL,
  `status` int(2) DEFAULT '0',
  `node` int(11) NOT NULL,
  `create_date` datetime DEFAULT NULL,
  `modify_date` datetime DEFAULT NULL,
  PRIMARY KEY (`idscripts`),
  UNIQUE KEY `UK_name_node` (`name`,`node`),
  KEY `FK_author_idx` (`author`),
  KEY `FK_node_idx` (`node`),
  CONSTRAINT `FK_author` FOREIGN KEY (`author`) REFERENCES `users` (`idusers`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_node` FOREIGN KEY (`node`) REFERENCES `script_nodes` (`idscript_nodes`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

CREATE TABLE `sessions` (
  `idsessions` int(11) NOT NULL AUTO_INCREMENT COMMENT 'The pk for db internal use.',
  `session_id` varchar(45) DEFAULT NULL COMMENT 'The id of session generated externally, used to handle session by app.',
  `user_id` int(11) DEFAULT NULL,
  `start_time` datetime DEFAULT NULL,
  `status` varchar(5) DEFAULT 'OPEN',
  PRIMARY KEY (`idsessions`),
  KEY `FK_user_session_id` (`user_id`),
  CONSTRAINT `FK_user_session` FOREIGN KEY (`user_id`) REFERENCES `users` (`idusers`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=127 DEFAULT CHARSET=latin1;

CREATE TABLE `status` (
  `idstatus` int(11) NOT NULL AUTO_INCREMENT,
  `statuscol` varchar(45) NOT NULL,
  `createDate` datetime NOT NULL,
  PRIMARY KEY (`idstatus`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

DROP TRIGGER IF EXISTS U3DSchema.status_BINS;

CREATE TRIGGER `status_BINS` BEFORE INSERT ON `status` FOR EACH ROW
SET NEW.createDate=NOW();

CREATE TABLE `CMS` (
  `idCMS` int(11) NOT NULL AUTO_INCREMENT,
  `fileName` varchar(100) NOT NULL,
  `owner` varchar(45) DEFAULT NULL,
  `parentId` int(11) DEFAULT NULL,
  `type` varchar(45) DEFAULT NULL,
  `sizeInByte` int(11) DEFAULT NULL,
  `createDate` datetime DEFAULT NULL,
  `updateDate` datetime DEFAULT NULL,
  `externalId` varchar(45) DEFAULT NULL,
  `statusId` int(11) DEFAULT NULL,
  PRIMARY KEY (`idCMS`),
  UNIQUE KEY `uk_CMS_2_idx` (`fileName`,`parentId`,`owner`),
  KEY `fk_CMS_1_idx` (`parentId`),
  KEY `index4` (`externalId`),
  KEY `fk_CMS_2_idx` (`statusId`),
  CONSTRAINT `fk_CMS_1` FOREIGN KEY (`parentId`) REFERENCES `CMS` (`idCMS`) ON DELETE SET NULL ON UPDATE NO ACTION,
  CONSTRAINT `fk_CMS_2` FOREIGN KEY (`statusId`) REFERENCES `status` (`idstatus`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=159 DEFAULT CHARSET=latin1;

CREATE TABLE `Tokens` (
  `idTokens` int(11) NOT NULL AUTO_INCREMENT,
  `token` varchar(45) NOT NULL,
  `StartDate` datetime DEFAULT NULL,
  `EndDate` datetime DEFAULT NULL,
  `Rules` varchar(400) NOT NULL,
  PRIMARY KEY (`idTokens`),
  UNIQUE KEY `token_UNIQUE` (`token`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;
