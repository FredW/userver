
/*Set up the init data for the dev enviroment*/
/*Developer goole account: u3dapp/password13579 */
USE `U3DSchema`;

SET @guest_user_name = 'U3DGuest';
INSERT INTO `users` (`user_name`) VALUES (@guest_user_name);
SELECT idusers INTO @user_id from `users` WHERE user_name = @guest_user_name;
INSERT INTO `script_nodes` (`name`, `description`, `owner`) VALUES ('GuestRoot', 'The root node for the guest users', @user_id);

/*The following section is development enviroment specific*/
SET @dev_google_id = '112212226590441355807';
SET @dev_fb_id = '100007327994465';
SET @dev_user_name = 'U3Ddev';

INSERT INTO `users` (`user_name`) VALUES (@dev_user_name);
SELECT idusers INTO @user_id from `users` WHERE user_name = @dev_user_name;

INSERT INTO `ext_auth_ids` (`auth_host`, `id`, `user_id`) VALUES ('GG', @dev_google_id, @user_id);
INSERT INTO `ext_auth_ids` (`auth_host`, `id`, `user_id`) VALUES ('FB', @dev_fb_id, @user_id);

INSERT INTO `script_nodes` (`name`, `description`, `owner`) VALUES ('DevRoot', 'The root node for the developers', @user_id);

INSERT INTO `CMS` (`fileName`,`type`,`externalId`) VALUES ('root','Folder','root');
