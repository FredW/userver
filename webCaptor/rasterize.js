var page = require('webpage').create(),
    system = require('system'),
    address, output, size;

page.onResourceError = function(resourceError) {
    page.reason = resourceError.errorString;
    page.reason_url = resourceError.url;
};

if (system.args.length < 3 || system.args.length > 5) {
    console.log('Usage: rasterize.js URL filename [paperwidth*paperheight|paperformat] [zoom]');
    console.log('  paper (pdf output) examples: "5in*7.5in", "10cm*20cm", "A4", "Letter"');
    console.log('  image (png/jpg output) examples: "1920px" entire page, window width 1920px');
    console.log('                                   "800px*600px" window, clipped to 800x600');
    phantom.exit(1);
} else {
    address = system.args[1];
    output = system.args[2];
    page.viewportSize = { width: 600, height: 600 };
    if (system.args.length > 3 && system.args[2].substr(-4) === ".pdf") {
        size = system.args[3].split('*');
        page.paperSize = size.length === 2 ? { width: size[0], height: size[1], margin: '0px' }
                                           : { format: system.args[3], orientation: 'portrait', margin: '1cm' };
    } else if (system.args.length > 3 && system.args[3].substr(-2) === "px") {
        size = system.args[3].split('*');
        if (size.length === 2) {
            pageWidth = parseInt(size[0], 10);
            pageHeight = parseInt(size[1], 10);
            page.viewportSize = { width: pageWidth, height: pageHeight };
            page.clipRect = { top: 0, left: 0, width: pageWidth, height: pageHeight };
        } else {
            console.log("size:", system.args[3]);
            pageWidth = parseInt(system.args[3], 10);
            pageHeight = parseInt(pageWidth * 3/4, 10); // it's as good an assumption as any
            console.log ("pageHeight:",pageHeight);
            page.viewportSize = { width: pageWidth, height: pageHeight };
        }
    }
    if (system.args.length > 4) {
        page.zoomFactor = system.args[4];
    }

    //Debug
    page.onResourceRequested = function (request) {
        //system.stderr.writeLine('= onResourceRequested()');
    	console.log('= onResourceRequested()');
        //system.stderr.writeLine('  request: ' + JSON.stringify(request, undefined, 4));
        //console.log('  request: ' + JSON.stringify(request, undefined, 4));
    };
     
    page.onResourceReceived = function(response) {
        //system.stderr.writeLine('= onResourceReceived()' );
    	console.log('= onResourceReceived()');
        //system.stderr.writeLine('  id: ' + response.id + ', stage: "' + response.stage + '", response: ' + JSON.stringify(response));
        //console.log('  id: ' + response.id + ', stage: "' + response.stage + '", response: ' + JSON.stringify(response));
    };
     
    page.onLoadStarted = function() {
        //system.stderr.writeLine('= onLoadStarted()');
        console.log('= onLoadStarted()');
        var currentUrl = page.evaluate(function() {
            return window.location.href;
        });
        //system.stderr.writeLine('  leaving url: ' + currentUrl);
        console.log('  leaving url: ' + currentUrl);
    };
     
    page.onLoadFinished = function(status) {
        console.log('= onLoadFinished()');
        console.log('  status: ' + status);
    };
     
    page.onNavigationRequested = function(url, type, willNavigate, main) {
        console.log('= onNavigationRequested');
        console.log('  destination_url: ' + url);
        console.log('  type (cause): ' + type);
        console.log('  will navigate: ' + willNavigate);
        console.log('  from page\'s main frame: ' + main);
    };
     
    page.onResourceError = function(resourceError) {
        console.log('= onResourceError()');
        console.log('  - unable to load url: "' + resourceError.url + '"');
        console.log('  - error code: ' + resourceError.errorCode + ', description: ' + resourceError.errorString );
    };
     
    page.onError = function(msg, trace) {
        console.log('= onError()');
        var msgStack = ['  ERROR: ' + msg];
        if (trace) {
            msgStack.push('  TRACE:');
            trace.forEach(function(t) {
                msgStack.push('    -> ' + t.file + ': ' + t.line + (t.function ? ' (in function "' + t.function + '")' : ''));
            });
        }
        console.log(msgStack.join('\n'));
    };
    //Debug

    page.open(address, function (status) {
        if (status !== 'success') {
            //console.log('Unable to load the address! Status: '+status);
            console.log(
                    "Error opening url \"" + page.reason_url
                    + "\": " + page.reason
                );
            phantom.exit(1);
        } else {
            window.setTimeout(function () {
                page.render(output);
                phantom.exit();
            }, 5000);
        }
    });
}