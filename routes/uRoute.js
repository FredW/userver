var webCaptor='/webCaptor/';
var webCapturesStorage=webCaptor+'captures/';
exports.testUrl = function(req, res) {
    var result = {status: 'OK', msg: 'The testUrl returns successfully'};
    res.send(JSON.stringify(result));
};

exports.getSession = function(req, res) {
    
    verifyUserInsession(req, res, function(request, response, uId, con) {
        var result = {status: 'OK', msg: 'The testUrl returns successfully', sessionId: '1234567'};
        response.send(JSON.stringify(result));
        }, true)
};

verifyUserInsession = function(req, res, execution, createSession) {
    var query = require('url').parse(req.url, true).query;
    var sessionId = query.sessionid;
    var connection = getConnection();
    connection.query('select user_id from sessions where session_id = "' + sessionId + '"', function(err, docs) {
        if (err)
        {
            console.log("err=" + err);
            result = {status: 'ERROR', msg: 'Unable to find matching session, please re-login. ' + err};
            res.send(JSON.stringify(result));
        }
        if (docs.length == 0) {
          if ( createSession ) {
            createNewSession(req, res, null, function(res, sessionId) {
              result = {status: 'YEAH', msg: 'Session has been created : ' + sessionId};
              res.send(JSON.stringify(result));
        });
          }
          else{
            result = {status: 'ERROR', msg: 'Not in valid session. '};
            res.send(JSON.stringify(result));
          }
        }
        else{
        var userId = docs[0].user_id;
        if (execution)
            execution(req, res, userId, connection);
        }
    })
};

createNewSession = function(req, res, userId, callback){
        var connection = getConnection();
        var date = new Date();
        var random = Math.floor((Math.random() * 10));
        var sessionId = "" + date.getTime() + random;
        console.log("sessionId = " + sessionId);
        
        var createSessionSql = 'INSERT INTO sessions (session_id, start_time, user_id) ';
        var createSessionSqlParameters;
        if ( userId != null) {
            createSessionSql = createSessionSql+'VALUES (? , ?, ?);';
            createSessionSqlParameters = [sessionId, date, userId];
        }
        else{
            createSessionSql = createSessionSql+'SELECT ?, ?, idusers FROM users WHERE user_name = \'U3DGuest\';';
            createSessionSqlParameters = [sessionId, date];
        }
        connection.query(createSessionSql, createSessionSqlParameters, function(err, docs) {
            if (err)
            {
                console.log("err = " + err);
            }
            callback(res, sessionId);
        });
};

getRoodNode = function(uId, con, req, res, execution) {
    var result;
    con.query('select idscript_nodes nodeId, name, description from script_nodes where parent is null and owner = ' + uId, function(err, docs) {
        if (err)
        {
            console.log("Error when getting root node: " + err);
            result = {status: 'ERROR', msg: 'Unable to load the chiildern for node. ' + err};
            res.send(JSON.stringify(result));
        }
        result = {status: 'OK', children: docs[0]};

        if (execution)
            execution(req, res, uId, docs[0].nodeId, con);
    });
};

exports.createScript = function(req, res) {
    verifyUserInsession(req, res, function(request, response, uId, con) {
        var nodeId = req.body.node;

        if (!nodeId || nodeId == undefined)
        {
            getRoodNode(uId, con, req, res, createScriptWithNode);
        }
        else
        {
            node = {nodeId: nodeId}
            createScriptWithNode(req, res, uId, node.nodeId, con);
        }
    });
};

createScriptWithNode = function(req, res, userId, nodeId, con) {
    var val = JSON.stringify(req.body.obj);
    var name = req.body.name;
    var description = req.body.description;
    var node = req.body.node;
    var result;
    var date = new Date();
    con.query('INSERT INTO scripts (author, script, name, description, node, create_date, modify_date) VALUES (?, ?, ?, ?, ?, ?, ?);', [userId, val, name, description, nodeId, date, date], function(err, docs) {
        if (err)
        {
            console.log("err=" + err);
            result = {status: 'ERROR', msg: 'Unable to save the script. ' + err};
            res.send(JSON.stringify(result));
        }
        result = {status: 'OK', msg: 'The script was created successfully', scriptId: docs.insertId};
        res.send(JSON.stringify(result));
    });
};

exports.updateScript = function(req, res) {
    verifyUserInsession(req, res, function(request, response, uId, con) {
        var val = JSON.stringify(req.body.obj);
        var name = req.body.name;
        var description = req.body.description;
        var scriptId = req.body.scriptId;
        var date = new Date();
        con.query('UPDATE scripts SET script = ?, name = ?, description = ?, modify_date = ? WHERE idscripts = ? and author = ?;', [val, name, description, date, scriptId, uId], function(err, docs) {
            if (err)
            {
                console.log("Error on updating script: " + err);
                result = {status: 'ERROR', msg: 'Unable to update the script. ' + err};
                res.send(JSON.stringify(result));
            }
            result = {status: 'OK', msg: 'The script was updated successfully'};
            res.send(JSON.stringify(result));
        });
    });
};

exports.loadScript = function(req, res) {
    verifyUserInsession(req, res, function(request, response, uId, con) {
        var scriptId = req.body.scriptId;
        var result;
        con.query('select script, name, description, node from scripts where idscripts = ' + scriptId, function(err, docs) {
            if (err)
            {
                console.log("err=" + err);
                result = {status: 'ERROR', msg: 'Unable to load the script. ' + err};
                res.send(JSON.stringify(result));
            }
            var script = docs[0].script;
            var name = docs[0].name;
            var description = docs[0].description;
            var node = docs[0].node;
            result = {status: 'OK', msg: 'The script has been loaded successfully', obj: script, name: name, description: description, node: node};
            res.send(JSON.stringify(result));
        });
    });
};

exports.loadUpdatedScript = function(req, res) {
    verifyUserInsession(req, res, function(request, response, uId, con) {
        var scriptId = req.body.scriptId;
        var lastQuerytimeUTC = req.body.lastQuerytime || 'Jan 1, 1970';
        var lastQuerytime = new Date(Date.parse(lastQuerytimeUTC));
        var now = new Date();
        var date = now.toUTCString();
        console.log("Query updated script for " + scriptId + " at " + now);
        var result;
        con.query('select script, name, description, node from scripts where idscripts = ? and modify_date > ?', [scriptId, lastQuerytime], function(err, docs) {
            if (err)
            {
                console.log("err=" + err);
                result = {status: 'ERROR', msg: 'Unable to save the script. ' + err};
                res.send(JSON.stringify(result));
            }
            if (docs.length > 0)
            {
                var script = docs[0].script;
                var name = docs[0].name;
                var description = docs[0].description;
                var node = docs[0].node;
                result = {status: 'OK', msg: 'The script has been loaded successfully', obj: script, name: name, description: description, node: node, lastQuerytime: date};
            }
            else
            {
                result = {status: 'NOTFOUND', msg: 'The script is either not found or not updated since last time check.', lastQuerytime: date};
            }
            res.send(JSON.stringify(result));
        });
    });
};

exports.addNode = function(req, res) {
    verifyUserInsession(req, res, function(request, response, uId, con) {
        var parentId = req.body.parentId;
        var name = req.body.name;
        var description = req.body.description;
        con.query('select 1 from script_nodes where idscript_nodes = ' + parentId + ' and owner = ' + uId, function(err, docs) {
            if (err)
            {
                console.log("err=" + err);
                result = {status: 'ERROR', msg: 'Adding node has been failed during owner validation. ' + err};
                res.send(JSON.stringify(result));
            }
            var date = new Date();
            con.query('INSERT INTO script_nodes (name, description, parent, owner) VALUES (? , ?, ?, ?);', [name, description, parentId, uId], function(err, docs) {
                if (err)
                {
                    console.log("err=" + err);
                    result = {status: 'ERROR', msg: 'Unable to add node. ' + err};
                    res.send(JSON.stringify(result));
                }
                result = {status: 'OK', msg: 'The node has been added successfully', nodeId: docs.insertId};
                res.send(JSON.stringify(result));
            });
        });
    });
};

exports.loadMyRoot = function(req, res) {
    verifyUserInsession(req, res, function(request, response, uId, con) {
        var result = getRoodNode(uId, con);
        res.send(JSON.stringify(result));
    });
};
exports.loadNodeChildren = function(req, res) {
    verifyUserInsession(req, res, function(request, response, uId, con) {
        var parentId = req.body.parentId;
        var result;
        con.query('select idscript_nodes nodeId, name, description from script_nodes where parent = ' + parentId, function(err, docs) {
            if (err)
            {
                console.log("err=" + err);
                result = {status: 'ERROR', msg: 'Unable to load the chiildern for node. ' + err};
                res.send(JSON.stringify(result));
            }
            result = {status: 'OK', children: docs};
            res.send(JSON.stringify(result));
        });
    });
};

exports.loadNodeScripts = function(req, res) {
    verifyUserInsession(req, res, function(request, response, uId, con) {
        var parentId = req.body.parentId;
        var result;
        con.query('select idscripts scriptId, name, description from scripts where node = ' + parentId, function(err, docs) {
            if (err)
            {
                console.log("err=" + err);
                result = {status: 'ERROR', msg: 'Unable to load the scripts for node. ' + err};
                res.send(JSON.stringify(result));
            }
            result = {status: 'OK', children: docs};
            res.send(JSON.stringify(result));
        });
    });
};

getWebCapture = function(req, res){
	var sizeList = [{"width": 1024, "height": 768, "index": 1},
			        {"width": 1152, "height": 864, "index": 2}, //The defaule
			        {"width": 1280, "height": 600, "index": 3},
	         		{"width": 1280, "height": 720, "index": 4},
			        {"width": 1280, "height": 768, "index": 5},
			        {"width": 1280, "height": 800, "index": 6},
			        {"width": 1280, "height": 854, "index": 7},
			        {"width": 1280, "height": 960, "index": 8},
			        {"width": 1280, "height": 1024, "index": 9},
			        {"width": 1360, "height": 768, "index": 10},
			        {"width": 1440, "height": 900, "index": 11},
			        {"width": 1600, "height": 900, "index": 12},
			        {"width": 1600, "height": 1200, "index": 13},
			        {"width": 1680, "height": 1050, "index": 14},
			        {"width": 1920, "height": 1080, "index": 15},
			        {"width": 1920, "height": 1200, "index": 16}];

	var MD5 = require('../util/MD5.js');
	var fs = require('fs');
	var http = require('http');
	var localWebcaptureURL = 'http://localhost:9090/getWebCapture?';
	 var query = req.query;

	 var encodedUrl = query.url;
	 var refresh = query.refresh;
	 var size = query.size;
	 var sizeIndex = query.sizeIndex;
	 var zoom = query.zoom;
	 if(encodedUrl == "" || encodedUrl == null)
		 {
          var result = {status: 'ERROR', msg: 'the URL must be specified to cature a web page.'};
          res.send(JSON.stringify(result));
		 }
	 var pathBase = encodedUrl;
	 var picWidth, picHeight;
	 var sizeHasBeenSet = false;
	 var fullPage = false;
	 if(size != null && size.trim() != '')
	 {
		 var sizeArraey = size.split('*');
		 if ( sizeArraey.length === 2)
			 {
               var sizeWidth = parseInt(sizeArraey[0]);
               var sizeHeight = parseInt(sizeArraey[1]);
               
               if ( !isNaN(sizeWidth) && sizeWidth>0 && !isNaN(sizeHeight) && sizeHeight>0)
            	   {
            	     picWidth = sizeWidth;
            	     picHeight = sizeHeight;
            	     sizeHasBeenSet = true;
            	   }
			 }
	 }
	 if (!sizeHasBeenSet)
		 {
		   var finalSizeIndex = 2; //default
		   if (sizeIndex != null && sizeIndex.trim() != '')
			   {
			     finalSizeIndex = sizeIndex;
			     fullPage = (sizeIndex.toLowerCase() == 'fullpage');
			   }
		   if ( !fullPage )
			   {
      	         picWidth = sizeList[finalSizeIndex-1].width;
    	         picHeight = sizeList[finalSizeIndex-1].height;
    	         sizeHaBeenSet = true;
			   }
		 }
	 if(!fullPage)
	 {
		 pathBase= pathBase + 'width=' + picWidth + 'height=' + picHeight;
	 }

	 if(zoom != null && zoom.trim() != '')
	 {
    	pathBase = pathBase + 'zoom='+zoom;
	 }

	 var filename = MD5.hex_md5(pathBase);
	 var targetPath = getBasePath() + webCapturesStorage + filename+'.png';

	 if(!fs.existsSync(targetPath) || (refresh != null && refresh.toLowerCase() == 'true'))
		 {
		 var exec = require('child_process').exec;
		 var webCaptorJs = getBasePath() + webCaptor + 'rasterize.js';
		 var shellCommand = 'phantomjs '+webCaptorJs+' '+encodedUrl+' '+targetPath;
		 if (!fullPage)
			 {
			   shellCommand = shellCommand+' '+picWidth+'px*'+picHeight+'px ';
			 }
		 else
			 {
			   shellCommand = shellCommand+' sizePlaceHolder ';
			 }
		 if(zoom != null && zoom.trim() != '')
		 {
	    	var intZoom = parseFloat(zoom);
	    	shellCommand = shellCommand+intZoom;
		 }
		 exec(shellCommand, function(error, stdout, stderr) {
		     console.log('stdout: ' + stdout);
		     console.log('stderr: ' + stderr);
		     if (error !== null) {
		         console.log('exec error: ' + error);
				  result = {status: 'ERR', msg: 'Not able to capture web page due to the error '+error};
			      res.send(JSON.stringify(result));
		     }
		      res.sendfile(targetPath);
		 });
		 
		 }
	 else
         {
	    	 res.sendfile(targetPath);
         }
	 
};