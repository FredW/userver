var extIdPrifix = 'EF';
var filesStorage='/files/';
var FolderType = 'Folder';
var scriptFileType = "application/vm-script";
var defaultOwner = 'anonymous';

var fs = require('fs');

var SOFT_DELETED = 10;

exports.mkDir = function(req, res) {
    var jsonObj = getCMSPostJson(req);
    var folderName = jsonObj.folderName;
    var con = getConnection();
    var date = new Date();
    var type = FolderType;
    var thisOwner = getCMSPostOwner( req);
    
    performCMSOperationUponParent(req, con, 
    function( parentId, externalId, parentName, owner, type ){
        extFolderId = getNewExternalId();
    
    con.query('INSERT INTO CMS (fileName, owner, parentId, type, externalId, createDate, updateDate) VALUES (?, ?, ?, ?, ?, ?, ?);', [folderName, thisOwner, parentId, type, extFolderId, date, date], function(err, docs) {
            if (err)
            {
                console.log("err=" + err);
                result = {status: 'ERROR', msg: 'Unable to add node. ' + err};
                    res.send(JSON.stringify(result));
            }
                result = formSingleFileJsonStr('The folder '+folderName+' has been created successfully', folderName, thisOwner, extFolderId, parentId, type, '0' );
                res.send(result);
            });
   }, 
   function(err){
            result = formErrorMessageJson(err, 'The folder '+name+' could not be made.');
            res.send(result);
        })

};

exports.uploadfile = function(req, res) {
    var name = req.files.upload.name;
    var type = req.files.upload.type;
    var size = req.files.upload.size;
    var sourcePath = req.files.upload.path;

    var extFileId = getCMSPostFileId(req);
    var owner = getCMSPostOwner(req);
    var date = new Date();
    var createFile = false;
    if (extFileId == null)
    {
        extFileId = getNewExternalId();
        createFile = true;
    }
    var targetPath = getBasePath() +filesStorage+ extFileId+'_'+name;
    var con = getConnection();

    performCMSOperationUponParent(req, con, 
    function( parentId, externalId, fname, ftype ){
        if (createFile) {
           con.query('INSERT INTO CMS (fileName, owner, parentId, sizeInByte, type, externalId, createDate, updateDate) VALUES (?, ?, ?, ?, ?, ?, ?, ?);', [name, owner, parentId, size, type, extFileId, date, date], function(err, docs) {
             if (err)
            {
                console.log("err=" + err);
                result = formErrorMessageJson(err, 'Unable to add node.');
                res.send(result);
            }
                result = formSingleFileJsonStr('The file '+name+' has been uploaded successfully', name, owner, extFileId, parentId, type, size );
                res.send(result);
            });
         }
       else
       {
        locateAvailableCMSByExternalId (extFileId, con,
        function( fileId, externalId, previousName, owner, type ){
        var previousPath = getBasePath() +filesStorage+ extFileId+'_'+previousName;
        fs.unlink(previousPath, function() {
          console.log("Deleting the previous content");
         });
        
        con.query('UPDATE CMS SET fileName = ?, parentId = ?, sizeInByte = ?, type = ?, updateDate = ? WHERE externalId = ?;', [name, parentId, size, type, date, fileId], function(err, docs) {
            if (err)
            {
                console.log("err=" + err);
                result = {status: 'ERROR', msg: 'Unable to add node. ' + err};
                    res.send(JSON.stringify(result));
            }
 
                result = formSingleFileJsonStr('The file '+name+' has been uploaded successfully', name, owner, extFileId, parentId, type, size );
                res.send(result);
            })
        }
        , function(err){
            result = formErrorMessageJson(err, 'The file '+name+' could not be uploaded and updated.');
            res.send(result);
        })
    
    };
    
    fs.rename(sourcePath, targetPath, function(err) {
       if (err) throw err; 
        // delete the temporary file, so that the explicitly set temporary upload dir does not get filled with unwanted files 
        fs.unlink(sourcePath, function() {
         if (err) throw err;
         });
      });
    
    },
    function( error ){
        res.send(error);
        })
};

exports.postToken = function(req, res) {
	   var para = req.query.para;
	   var theToken = JSON.parse(para);
	   var requestedToken =  theToken.Token;
       if (requestedToken.length === 0 || !requestedToken.trim())
       {
    	    var date = new Date();
    	    var random = Math.floor((Math.random() * 10));
    	    theToken.Token = 'TK' + date.getTime() + random;
       }
       var rulesStr = JSON.stringify(theToken.rules);
       
       var con = getConnection();

       var postTokenSqlStatment = 'INSERT INTO Tokens (token, Rules, StartDate, EndDate) VALUES (?, ?, ?, ?);'
       con.query(postTokenSqlStatment, [theToken.Token, rulesStr, theToken.startDate, theToken.endDate], function(err, docs) {
       if (err)
         {
           console.log("err=" + err);
           result = {status: 'ERROR', msg: 'Unable to add node. ' + err};
           res.send(JSON.stringify(result));
         }
           result = {status: 'OK', postedToken: theToken};
           res.send(JSON.stringify(result));
         });
};

exports.deletefile = function(req, res) {
    var extFileId = getCMSPostFileId(req);
    var date = new Date();
    var con = getConnection();
    
    if ( extFileId.toLowerCase() == 'root' ) {
        var err = ' Try to delete root.';
        console.log("Err = "+err);
        result = formErrorMessageJson(err, 'The root cannot be deleted');
        res.send(result);
    }
    else{
    locateAvailableCMSByExternalId(extFileId, con,
        function( fileId, externalId, fileName, owner, type ){
            if ( type == FolderType ) {
            var queryStatment = 'select count(1) AS childrenCount from CMS where statusId IS NULL and parentId in';
             queryStatment +=' (select idCMS from CMS where externalId = \''+ externalId+'\')  AND statusId IS NULL';
             con.query(queryStatment, function(err, docs) {
             if (err)
             {
               console.log("err=" + err);
               result = {status: 'ERROR', msg: 'Unable to delete file ' + err};
               res.send(JSON.stringify(result));
             }else{
               var countOfChildren = docs[0].childrenCount;
               if (countOfChildren == '0') {
                 softDeleteCMSByExternalId(externalId, con, function(err){
                   console.log("err=" + err);
                   result = formErrorMessageJson(err, 'The folder '+fileName+' could not be deleted.');
                   res.send(result);
                 });
                 result = {status: 'OK', msg: fileName+' has been deleted.'};
                 res.send(JSON.stringify(result));
                }else{
                  var err2 = 'Could not delete folder.';
                  console.log("err= "+err2);
                  result = formErrorMessageJson(err2, 'The folder '+fileName+' has undeleted children.');
                  res.send(result);
                }
             }
            });
            }
            else{
              softDeleteCMSByExternalId(externalId, con, function(err){
                                                         console.log("err=" + err);
                                                         result = formErrorMessageJson(err, 'The file '+fileName+' could not be deleted.');
                                                         res.send(result);
                });
              result = {status: 'OK', msg: fileName+' has been deleted.'};
              res.send(JSON.stringify(result));
            }
        });
    }
};

exports.deleteTrash = function(req, res) {
	var con = getConnection();
    var queryStatment = 'select idCMS fileId, externalId externalId, fileName fileName, owner owner, type type from CMS where statusId ='+SOFT_DELETED+' and type <>\''+FolderType+'\'';
    con.query(queryStatment, function(err, docs) {
       if (err)
       {
           console.log("err=" + err);
           errorFunction(err);
       }
       else{
    	   var deletedFileIds = "(";
    	   for (var i=0,  tot=docs.length; i < tot; i++) {
               var fileId = docs[i].fileId;
               if ( i > 0)
            	   {
            	   deletedFileIds = deletedFileIds+', '
            	   }
               deletedFileIds = deletedFileIds+fileId;
               var externalId = docs[i].externalId;
               var fileName = docs[i].fileName;
               var owner = docs[i].owner;
               var type = docs[i].type;
               var targetPath = getBasePath() +filesStorage+ externalId+'_'+fileName;
               fs.unlink(targetPath, function() {
                if (err)
                	{
                	throw err;
                	}
              });
    		 }
    	   deletedFileIds = deletedFileIds+')';
    	   var idClause = ' OR idCMS in ';
    	   if (deletedFileIds.length > 2)
    		   {
    		    idClause = idClause+deletedFileIds;
    		   }
    	   else
    		   {
    		   idClause = "";
    		   }
    	   var removeDeletedFolders = 'delete from CMS where (type = \''+FolderType+'\' AND statusId = '+SOFT_DELETED+')'+idClause;
           con.query(removeDeletedFolders, function(err, docs) {
           if (err)
            {
              errorFunction(err);
            }
           })
           }
    
       })
};

exports.getExplorer = function(req, res){
  res.render('CMSExplorer', { title: 'CMSExplorer' });
};

exports.getScriptFilePicker = function (req, res){
     res.render('scriptFilePicker',
                { title: 'Script File Picker'});
};

exports.imgFilePicker = function (req, res){
     res.render('imgFilePicker',
                { title: 'Image File Picker'});
};

exports.getImgFileManager = function (req, res){
    var folderId = req.query.folder;
    var hasParent = true;
    
     if(folderId == "" || folderId == null)
       {
          folderId = 'root';
       }
      if ( folderId == 'root') {
         hasParent = false;
      }
    if (hasParent) {
    var con = getConnection();
    var queryStatment = 'select idCMS fileId, externalId externalId, fileName fileName, type type from CMS where idCMS in';
     queryStatment +=' (select parentId from CMS where externalId = \''+ folderId+'\')  AND statusId IS NULL';
     con.query(queryStatment, function(err, docs) {
        if (err)
        {
            console.log("err=" + err);
            result = {status: 'ERROR', msg: 'Unable to add node. ' + err};
            res.send(JSON.stringify(result));
        }
        else{
            var parentId = docs[0].externalId;
            var pHref = '/getCMSExplorer/imgFileManager?folder='+parentId;
            res.render('imgFileManager',
            { title: 'Image File Manager', onloadScript: 'loadChildren( \''+folderId+'\' );', hasParent: true, parentHref: pHref});
        }
     });
    }else{
            res.render('imgFileManager',
            { title: 'Image File Manager', onloadScript: 'loadChildren( \''+folderId+'\' );'});
    }
};

exports.postScript = function(req, res){
        var con = getConnection();
        var fileObject = getFileObjectFromrequest( req );
        fileObject.extFileId = getNewExternalId();
        var owner = getCMSPostOwner(req);
        date = new Date();
        var postScriptSqlStatment = 'INSERT INTO CMS ( fileName, owner, parentId, type, externalId, sizeInByte, createDate, updateDate) '+
                                    ' SELECT ?, ?, idCMS, ?, ?, ?, ?, ? '+
                                    ' FROM CMS parent WHERE parent.externalId = ? AND parent.statusId is NULL;'
        con.query(postScriptSqlStatment, [fileObject.fileName, owner, fileObject.type, fileObject.extFileId, fileObject.size, date, date, fileObject.folderId], function(err, docs) {
            if (err)
            {
                console.log("err=" + err);
                result = {status: 'ERROR', msg: 'Unable to add node. ' + err};
                    res.send(JSON.stringify(result));
            }
                saveScriptFile(fileObject, req, res);
                result = formSingleFileJsonStr('The file '+fileObject.fileName+' has been updated successfully', fileObject.fileName, owner, fileObject.extFileId, fileObject.folderId, fileObject.type, fileObject.size );
                res.send(result);
            });
};

exports.putScript = function(req, res){
        var con = getConnection();
        var owner = getCMSPostOwner(req);
        var fileObject = getFileObjectFromrequest( req );
        date = new Date();
        var selectfileQuery = 'select file.idCMS id, file.externalId externalId from CMS file, CMS folder where file.fileName = ? '
                             +' AND file.parentId = folder.idCMS AND folder.externalId = ? AND owner = ? AND file.statusId IS NULL AND folder.statusId IS NULL;';
        con.query(selectfileQuery, [fileObject.fileName, fileObject.folderId, owner], function(err, docs) {
        if (err)
        {
            console.log("err=" + err);
        }
        else{
            var externalId = docs[0].externalId;
            var Id = docs[0].id;
            fileObject.extFileId = externalId;
            var putScriptSqlStatment = 'UPDATE CMS SET sizeInByte = ?, updateDate = ? WHERE idCMS = ?;'
        
            con.query(putScriptSqlStatment, [fileObject.size, date, Id], function(err, docs) {
            if (err)
            {
                console.log("err=" + err);
                result = {status: 'ERROR', msg: 'Unable to add node. ' + err};
                res.send(JSON.stringify(result));
            }
                saveScriptFile(fileObject,req, res);
                result = formSingleFileJsonStr('The file '+fileObject.fileName+' has been updated successfully', fileObject.fileName, externalId, fileObject.folderId, fileObject.type, fileObject.size );
                res.send(result);
            })
        }
        
        })
};

exports.putCMSLocks = function(req, res){
    var filesIdStr = '';
    var files = getCMSPostJson( req ).files;
    for (var i = 0; i < files.length; i++)
    {
        if ( i>0 ) {
            filesIdStr +=', ';
        }
        filesIdStr +='#'+files[i]+'#';
    }
    var con = getConnection();
    var lockCMSQuery = 'SELECT lock_CMS_items( ? ) lockId';
    con.query(lockCMSQuery, [filesIdStr], function(err, docs) {
        if (err)
        {
            console.log("err=" + err);
            result = {status: 'ERROR', MSG: 'There\'s error occured during locking the CMS items for deleting: '+err};
            res.send(JSON.stringify(result));
        }
        else{
            var statusId = docs[0].lockId;
            result = {status: 'OK', statusId: statusId};
            res.send(JSON.stringify(result));
           }   
        })
        
};

getFileObjectFromrequest = function( req )
{
   var para = req.query.para;
   var jsonObj = JSON.parse(para);
   var fileObj = new Object();
   fileObj.fileName = jsonObj.fileName;
   fileObj.content = jsonObj.scriptBody;
   fileObj.folderId = jsonObj.folderId;
   fileObj.size = fileObj.content.length;
   fileObj.type = jsonObj.type;
   return fileObj;
};

saveScriptFile = function(fileObject, req, res)
{
    var targetPath = getBasePath() +filesStorage+ fileObject.extFileId+'_'+fileObject.fileName;
    buffer = new Buffer(fileObject.content);

    fs.open(targetPath, 'w', function(err, fd) {
    if (err) {
        throw 'error opening file: ' + err;
        } else {
        fs.write(fd, buffer, 0, buffer.length, null, function(err) {
        if (err) throw 'error writing file: ' + err;
            fs.close(fd, function() {
            console.log('file written');
            })
          });
        }
    });
};

getFile = function(req, res){
    var extId = req.params.extid;
    var con = getConnection();
    locateAvailableCMSByExternalId(extId, con, 
                          function( fileId, externalId, fileName, owner, type ){
                            if ( type == FolderType ) {
                                getCMSChildrenByParentId(extId, con, 
                                   function( files ){
                                   result = {status: 'OK', msg: 'The children have been loaded succssfully.', filesJson: files};
                                   res.send(JSON.stringify(result));
                                 },
                                function(err){
                                  result = formErrorMessageJson(err, 'The children could not be fetched.');
                                  res.send(result);
                              });
                            }
                            else{
                            var filePath = getBasePath() +filesStorage+ extId+'_'+fileName;
                            res.sendfile(filePath);
                            }
                          },
                          function(err){
                          result = formErrorMessageJson(err, 'The file '+fileName+' could not got.');
                          res.send(result);
        });
};

getChildren = function(req, res){
    var extId = req.params.extid;
    var jsonObj = getCMSPostJson( req);
    var type = jsonObj.type;
    var con = getConnection();
    getCMSChildrenByParentId(extId, type, con, 
                          function( files ){
                           result = {status: 'OK', msg: 'The children have been loaded succssfully.', filesJson: files};
                           res.send(JSON.stringify(result));
                          },
                          function(err){
                          result = formErrorMessageJson(err, 'The children could not be fetched.');
                          res.send(result);
        });
};

getFileByPath = function(req, res){
    var jsonObj = getCMSPostJson( req);
    var fullpath = jsonObj.fullpath;
    var fullpathObj = JSON.parse(fullpath);
    var path = fullpathObj.path;
    var folderId = fullpathObj.fromFolderId;
    if ( folderId == null || folderId.length == 0) {
    	folderId = 'root';
    }
    var con = getConnection();
    _getFileByPath(folderId, path, con, res);
};

_getFileByPath = function(folderExtId, path, con, res){
	var child = path.child;
	var name = path.name;
	var owner = path.owner;
    if ( owner == null || owner.length == 0) {
    	owner = defaultOwner;
    }
	if (child == null) {
	     var queryStatment = 'select idCMS fileId, externalId externalId, fileName fileName, owner owner, type type, sizeInByte size from CMS '
	    	 +'where statusId IS NULL and fileName = ? and owner = ? and parentId in (select idCMS from CMS where externalId = ?);';
	     con.query(queryStatment, [name, owner, folderExtId], function(err, docs) {
	        if (err) {
	            console.log("err=" + err);
	            result = {status: 'ERROR', msg: 'Unable to locate file.', error: err};
	            res.send(result);
	        }
	        else{
	        	var result = {status: 'ERROR', msg: 'Unable to locate file '+name};
	        
	        	if (typeof docs[0] !== 'undefined' && docs[docs] !== null) 
        		{
	             var fileId = docs[0].fileId;
	             var fileExternalId = docs[0].externalId;
	             var fileName = docs[0].fileName;
	             var fileOwner = docs[0].owner;
	             var type = docs[0].type;
	             var size = docs[0].size;
	             result = formSingleFileJsonStr('The file '+fileName+' has been located', fileName, fileOwner, fileExternalId, folderExtId, type, size );
        		}
	            res.send(result);
	        }
	     })
		}
	else
		{
           var selectFolderQuery = 'select externalId externalId from CMS where statusId IS NULL and fileName = ? and owner = ? and '
        	   +'parentId in (select idCMS from CMS where externalId = ?);';
	        con.query(selectFolderQuery, [name, owner, folderExtId], function(err, docs) {//8
           if (err) {
              console.log("err=" + err);
              result = {status: 'ERROR', msg: 'Unable to locate folder in path.', error: err};
              res.send(result);
           }
           else{
	        	if (typeof docs[0] !== 'undefined' && docs[docs] !== null) 
        		{
                   var subFolderExtId = docs[0].externalId;
                  _getFileByPath(subFolderExtId, child, con, res);
        		}else{
                    console.log("err=" + err);
                    result = {status: 'ERROR', msg: 'Unable to locate folder in path.', error: err};
                    res.send(result);
        		}
           }
		  })
		}
	};

getToken = function(req, res){
	var jsonObj = getCMSPostJson( req);
	var token = jsonObj.Token;
    var con = getConnection();
	_getToken(token, function(data){
		result = {status: 'OK', tokenObj: data};
		res.send(result);
	    }, 
	    function(err){
            result = formErrorMessageJson(err, 'The file '+name+' could not be uploaded and updated.');
            res.send(result);
        }, con);
	};

	_getToken = function(token, callback, errorFunction, con){
    var queryStatment = 'SELECT token Token, StartDate startDate, EndDate endDate, Rules rules FROM Tokens WHERE token = ?;';
    con.query(queryStatment, [token], function(err, docs) {
        if (err)
        {
            console.log("err=" + err);
            errorFunction(err);
        }
        else{
            callback( docs[0]);
        }
    });
	};

getCMSPostJson = function (req){
    var query = req.query;
    var para = query.para;
    return JSON.parse(para);
};

getCMSPostFileId = function (req){
    var jsonObj = getCMSPostJson( req);
    return jsonObj.fileId;
};

getCMSPostParentId = function (req){
    var jsonObj = getCMSPostJson( req);
    var parentId = jsonObj.parentId;
    if ( parentId == null || parentId.length == 0) {
        parentId = 'root';
    }
    return parentId;
};

getCMSPostOwner = function (req){
    var jsonObj = getCMSPostJson( req);
    var owner = jsonObj.owner;
    if ( owner == null || owner.length == 0) {
    	owner = defaultOwner;
    }
    return owner.trim();
};

performCMSOperationUponParent = function (req, con, operation, errorCallback){
    var parentExternalId = getCMSPostParentId(req);
    locateAvailableCMSByExternalId(parentExternalId, con, operation, function(err){
        console.log("err=" + err);
        result = {status: 'ERROR', msg: 'Unable to locate the parent folder.'};
        errorCallback(JSON.stringify(result));
        });
};

locateAvailableCMSByExternalId = function(externalId, con, operation, errorFunction){
     var queryStatment = 'select idCMS fileId, externalId externalId, fileName fileName, owner owner, type type from CMS where externalId = \'' + externalId+'\' AND statusId IS NULL';
     con.query(queryStatment, function(err, docs) {
        if (err)
        {
            console.log("err=" + err);
            errorFunction(err);
        }
        else{
            var fileId = docs[0].fileId;
            var externalId = docs[0].externalId;
            var fileName = docs[0].fileName;
            var owner = docs[0].owner;
            var type = docs[0].type;
            operation( fileId, externalId, fileName, owner, type );
        }
     })
};

softDeleteCMSByExternalId = function(externalId, con, errorFunction){
    var date = new Date();
     var queryStatment = 'update CMS set statusId = '+SOFT_DELETED+', owner = '+date.getTime()+' where externalId = \'' + externalId+'\'';
     con.query(queryStatment, function(err, rows) {
        if (err)
        {
            console.log("err=" + err);
            errorFunction(err);
        }
     })
};

getCMSChildrenByParentId = function(parentId, type, con, operation, errorFunction){
    var queryStatment = "select externalId fileId, fileName fileName, owner owner, type type, parentId parentId, sizeInByte size "
               +"from CMS where parentId in (select idCMS from CMS where statusId IS NULL AND externalId ='" + parentId+"') AND statusId IS NULL ";

    if ( !(type == null) && !(type.length == 0)) {
      if ( type == 'image') {
          queryStatment += "and (type like 'image%' or type = '"+FolderType+"')";
        }else{
          queryStatment += "and type in ('"+type+"', '"+FolderType+"')";
        }
    }
     con.query(queryStatment,
               function(err, docs) {
        if (err)
        {
            console.log("err=" + err);
            errorFunction(err);
        }
        else{
            var files = [];
            for (var i = 0; i < docs.length; i++) {
               fileId = docs[i].fileId;
               fileName = docs[i].fileName;
               parentId = parentId;
               type = docs[i].type;
               size = docs[i].size;
               owner = docs[i].owner;
               fileJson = formFileJson( fileName, owner, fileId, parentId, type, size );
               files.push(fileJson);
            }
             operation( files );
        }
     })
};

getNewExternalId = function(){
    var date = new Date();
    var random = Math.floor((Math.random() * 10));
    return extIdPrifix + date.getTime() + random;
};
        
formFileJson = function (name, owner, fileId, parentId, type, size){
    return {fileId: fileId, owner: owner, parentId: parentId, filename: name, fileType: type, fileSize: size};
};

formSingleFileJsonStr = function( msg, name, owner, fileId, parentId, type, size ){
                fileJson = formFileJson( name, owner, fileId, parentId, type, size );
                result = {status: 'OK', msg: msg, fileJson: fileJson};
                return JSON.stringify(result);
};

formErrorMessageJson = function(err, msg){
            result = {status: 'ERROR', msg: msg, error: err};
            return JSON.stringify(result);
        }