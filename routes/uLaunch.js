/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
exports.launch = function(req, res, session) {
    res.render('launch', {title: 'Express', sessionId: session});
};

exports.view = function(req, res, session) {
    var userInfoHash = getUserInfoHash(req);
    var scriptId;
    if (global.requiredScriptIds != undefined && global.requiredScriptIds.hasOwnProperty(userInfoHash))
    {
        scriptId = global.requiredScriptIds[userInfoHash];
        delete global.requiredScriptIds[userInfoHash];
    }
    else
        {
    //Temporary hard code session for demo which doesn't require FB auth
            var query = require('url').parse(req.url, true).query;
            scriptId = query.scriptId;
        }
    res.render('view', {title: 'Express', sessionId: session, scriptId: scriptId, viewerInterval: getViewerInterval()});
};

authUserByGoogleId = function(googleId, callback, req, res) {
    executeWithAuth(googleId, 'GG', callback, req, res);
};

executeWithFBAuth = function(fbId, callback, req, res) {
    executeWithAuth(fbId, 'FB', callback, req, res);
};
executeWithAuth = function(auth_id, auth_host, callback, req, res) {
    var connection = getConnection();
    connection.query('select * from users u, ext_auth_ids e where u.idusers = e.user_id and e.auth_host = "' + auth_host + '" and e.id = "' + auth_id + '"', function(err, docs) {
        if (err)
        {
            console.log("err=" + err);
        }
        console.log("user=" + docs[0].user_name);
        var date = new Date();
        var random = Math.floor((Math.random() * 10));
        var sessionId = "" + date.getTime() + random;
        console.log("sessionId = " + sessionId);
        connection.query('INSERT INTO sessions (session_id, user_id, start_time) VALUES (? , ?, ?);', [sessionId, docs[0].idusers, date], function(err, docs) {
            if (err)
            {
                console.log("err = " + err);
            }
            callback(req, res, sessionId);
        });
    });
};
